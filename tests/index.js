var tape = require("tape"),
    fs = require("fs"),
    gulp = require("gulp"),
    through = require("through2"),
    fileUtils = require("@nathanfaucett/file_utils"),
    path = require("path"),
    localesBundler = require("..");

function bundle(out, flatten, minify, callback) {
    var build = path.join(__dirname, path.join(out, "locales"));

    if (fs.existsSync(build)) {
        fileUtils.removeSync(build);
    }

    gulp
        .src(path.join(__dirname, path.join("locales", "**", "*.json")))
        .pipe(localesBundler({flatten: flatten, minify: minify}))
        .pipe(gulp.dest(build))
        .pipe(
            through.obj(
                function(chunk, enc, callback) {
                    callback(undefined, chunk);
                },
                function() {
                    callback();
                }
            )
        );
}

tape("locales bundler flatten and minify", function(assert) {
    bundle("build/0", true, true, assert.end);
});
tape("locales bundler minify", function(assert) {
    bundle("build/1", false, true, assert.end);
});
tape("locales bundler flatten", function(assert) {
    bundle("build/2", true, false, assert.end);
});
tape("locales bundler", function(assert) {
    bundle("build/3", false, false, assert.end);
});
