var path = require("path"),
    File = require("vinyl"),
    extend = require("@nathanfaucett/extend"),
    flattenObject = require("@nathanfaucett/flatten_object"),
    objectForEach = require("@nathanfaucett/object-for_each"),
    arrayForEach = require("@nathanfaucett/array-for_each"),
    through = require("through2");

module.exports = localesBundler;

function localesBundler(options) {
    var locales = [];

    options = options || {};

    options.flatten = options.flatten === true;
    options.minify = options.minify === true;

    return through.obj(
        function(file, enc, callback) {
            var relativePath = path.relative(
                    file.base,
                    path.dirname(file.path)
                ),
                paths = relativePath.split(path.sep),
                locale = paths.shift(),
                key = path.basename(file.path, path.extname(file.path)),
                json;

            try {
                json = JSON.parse(file.contents);
            } catch (e) {
                return callback(e);
            }

            arrayForEach(paths, function each(key) {
                var out = {};
                out[key] = json;
                json = out;
            });

            if (options.flatten) {
                json = flattenObject(json);
            }

            locales.push({
                locale: locale,
                key: key,
                messages: json
            });

            callback();
        },
        function(callback) {
            var _this = this,
                out = createLocales(options, locales);

            objectForEach(out, function each(locales, locale) {
                var fileName = locale + ".json",
                    contents;

                if (options.minify) {
                    contents = JSON.stringify(locales);
                } else {
                    contents = JSON.stringify(locales, null, 2);
                }

                _this.push(
                    new File({
                        path: fileName,
                        contents: new Buffer(contents)
                    })
                );
            });

            callback();
        }
    );
}

function createLocales(options, locales) {
    var out = {};

    arrayForEach(locales, function each(chunk) {
        var locale = chunk.locale,
            outLocale = out[locale] || (out[locale] = {});

        if (options.flatten) {
            extendLocalesFlatten(outLocale, chunk);
        } else {
            extendLocales(outLocale, chunk);
        }
    });

    return out;
}

function extendLocalesFlatten(out, chunk) {
    var key = chunk.key,
        messages = chunk.messages;

    for (var k in messages) {
        out[key + "." + k] = messages[k];
    }
}

function extendLocales(out, chunk) {
    out[chunk.key] = extend({}, chunk.messages);
}
